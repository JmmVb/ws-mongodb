package com.example.mongo.test;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.example.mongo.bean.Client;
import com.example.mongo.controller.ClientController;

import static org.junit.Assert.assertEquals;

public class ClientTest {

	Client client;
	ClientController clientController;
	
	@Before
	public void setUp() {

		client = new Client();
		client.setGender("M");
		client.setLastName("Valdez");
		client.setName("Jimmy");
		client.setNumberOfDocument("12345678");
		client.setPhone("123456789");
		clientController = new ClientController();
	}

	@Ignore
	@Test
	public void shouldSaveClient() {
		String response = clientController.save(client);
		assertEquals("1", response);
	}

	@Ignore
	@Test
	public void shouldUpdateClient(){
		client = new Client();
		client.setNumberOfDocument("12345678");
		client.setName("Richard");
		client.setLastName("Blas");
		String response = clientController.update(client);
		assertEquals("1", response);
		
	}
}
