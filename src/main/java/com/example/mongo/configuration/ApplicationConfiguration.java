package com.example.mongo.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by Jimmy on 06/05/2015.
 */
@Configuration
@ComponentScan("com.example.mongo")
@Import({ WebInitializer.class, MongoDBConfiguration.class })
public class ApplicationConfiguration {
}
