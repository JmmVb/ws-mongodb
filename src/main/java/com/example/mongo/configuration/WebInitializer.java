package com.example.mongo.configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;

/**
 * Created by Jimmy on 06/05/2015.
 */
@Configuration
public class WebInitializer implements WebApplicationInitializer {

	public void onStartup(ServletContext servletContext)
			throws ServletException {

		ServletRegistration.Dynamic servletRegistration = servletContext
				.addServlet("conexion-mongodb", new CXFServlet());
		servletRegistration.addMapping("/*");
		servletRegistration.setLoadOnStartup(1);

	}

}
