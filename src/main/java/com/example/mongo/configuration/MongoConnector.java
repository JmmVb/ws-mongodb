package com.example.mongo.configuration;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;

/**
 * Created by Jimmy on 02/06/2015.
 */
public class MongoConnector {

	public MongoConnector() {
	}

	private static class LazyHolder {

		public static final Mongo INSTANCE;
		
		static {
			Mongo temp = null;
			try {

				temp = new MongoClient("127.0.0.1");
			} catch (Exception e) {
				e.printStackTrace();
			}
			INSTANCE = temp;
		}
		
	}

	public static Mongo getInstance() {
		return LazyHolder.INSTANCE;
	}
}
