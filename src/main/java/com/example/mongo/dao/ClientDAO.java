package com.example.mongo.dao;

import com.example.mongo.bean.Client;

public interface ClientDAO {

	String insertClient(Client client);

	String updateClient(com.example.mongo.model.Client clientRQ);
}
