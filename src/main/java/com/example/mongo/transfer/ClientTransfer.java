package com.example.mongo.transfer;

import com.example.mongo.model.Client;

public class ClientTransfer {

	public Client buildClientUpdate(com.example.mongo.bean.Client client){
		
		Client clientModel = new Client();
		try { 
			clientModel.setNumberOfDocument(client.getNumberOfDocument());
			clientModel.setName(client.getName());
			clientModel.setLastName(client.getLastName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return clientModel;
	} 
} 
