package com.example.mongo.ws;

import javax.jws.WebService;

import com.example.mongo.bean.Client;
import com.example.mongo.controller.ClientController;

@WebService(endpointInterface = "com.example.mongo.ws.ClientConnector",serviceName = "ClientConnector")
public class ClientConnectorImpl implements ClientConnector{

	@Override
	public String save(Client client) {
		ClientController clientController = new ClientController();
		return clientController.save(client);
	}

	@Override
	public String update(Client client) {
		ClientController clientController = new ClientController();
		return clientController.update(client);
	}

	
	
}
