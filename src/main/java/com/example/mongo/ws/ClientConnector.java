package com.example.mongo.ws;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.example.mongo.bean.Client;

@WebService
public interface ClientConnector {

	@SOAPBinding(style = SOAPBinding.Style.RPC, use = SOAPBinding.Use.LITERAL)
	String save(@WebParam(name="client")Client client);
	
	@SOAPBinding(style = SOAPBinding.Style.RPC, use = SOAPBinding.Use.LITERAL)
	String update(@WebParam(name="client")Client client);
}
